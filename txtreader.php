<?php
$filePath="text.txt";
$count=0;
//$regexp='/(\[(?:\[??[^\[]*?\]))/';
//$regexp="/\[(.*?)\]/";
$regexp="/\[((?:[^][]++|(?R))*)]/";
$newStr="";

if (file_exists($filePath)) {
    echo "Reading file $filePath<br><br>";
    echo "Finding square brackets on file...<br><br>";

    $fh = fopen($filePath,'r');
    while ($line = fgets($fh)) {

        if(preg_match($regexp,$line,$result,PREG_OFFSET_CAPTURE)){
            //echo($line)."<br>";
            echo "found ".$result[0][0]." on line: $count and column: ".$result[0][1]."<br>";
            //    
            $newStr.="$count |".str_replace($result[0][0],"<mark>".$result[0][0]."</mark>",$line)."<br>";
        //echo print_r($result)."<br>"; 
         
        }else{
            $newStr.="$count |$line"."<br>";
        }

        $count++;
    }
    fclose($fh);
    echo "<br>So, the file: <br><br>".$newStr;
}else{
    echo "file $filePath not found";
}
?>